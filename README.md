# Hello! <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

[![Visitor](https://visitor-badge.laobi.icu/badge?page_id=LightLegendXR.LightLegendXR)](https://github.com/AbOuLfOoOoOuF) [![GitHub followers](https://img.shields.io/github/followers/LightLegendXR.svg?style=social&label=Follow)](https://github.com/LightLegendXR?tab=followers)

![LightLegendXR's Github stats](https://github-readme-stats.vercel.app/api?username=LightLegendXR&show_icons=true&theme=chartreuse-dark&hide_border=true)

[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com?user=LightLegendXR&theme=chartreuse-dark&hide_border=true)](https://git.io/streak-stats)

[![About Me/Links](https://lightlegendxr.github.io)](https://lightlegendxr.github.io/)

<h2> Connect with me <img src='https://raw.githubusercontent.com/ShahriarShafin/ShahriarShafin/main/Assets/handshake.gif' width="100px"> </h2>

[![Telegram Badge](https://img.shields.io/badge/-@LightLegendXR-0088CC?style=flat&logo=Telegram&logoColor=white)](https://t.me/LightLegendXR "Contact on Telegram")



